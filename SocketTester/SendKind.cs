﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketTester
{
    /// <summary>
    /// 送信種別
    /// </summary>
    public enum SendKind
    {
        /// <summary>不明</summary>
        Unknown,
        /// <summary>バイナリ送信</summary>
        Binary,
        /// <summary>16進数送信</summary>
        HexaDecimal,
        /// <summary>テキスト(UTF-8)送信</summary>
        TextUtf8,
        /// <summary>テキスト(Shift_JIS)送信</summary>
        TextSjis,
    }


    /// <summary>
    /// 送信種別拡張クラス
    /// </summary>
    public static class SendKindExt
    {
        /// <summary>
        /// 文字列を対応するバイナリデータに変換します。
        /// </summary>
        /// <param name="kind"></param>
        /// <param name="baseText"></param>
        /// <returns></returns>
        public static byte[] ConvertToData(this SendKind kind, string baseText)
        {
            if (string.IsNullOrEmpty(baseText)) throw new ArgumentNullException(nameof(baseText));

            switch (kind)
            {
                case SendKind.Binary:
                    if (baseText.Length % 8 > 0)
                    {
                        throw new InvalidOperationException("2進送信の桁が不正 8文字単位にしてください。文字長=" + baseText.Length);
                    }
                    var ret = new byte[baseText.Length / 8];
                    for (int idx = 0; idx < baseText.Length; idx++)
                    {
                        var byteIdx = idx / 8;
                        ret[byteIdx] = (byte)( (ret[byteIdx] << 1) + (baseText[idx] - '0') );

                    }
                    return ret;
                case SendKind.HexaDecimal:
                    if (baseText.Length % 2 > 0)
                    {
                        throw new InvalidOperationException("16進送信の桁が不正 2文字単位にしてください。");
                    }
                    ret = new byte[baseText.Length / 2];
                    for (int idx = 0; idx < baseText.Length; idx += 2)
                    {
                        var hexStr = baseText.Substring(idx, 2);
                        int hexInt = int.Parse(hexStr, System.Globalization.NumberStyles.HexNumber);
                        int binaryIdx = idx / 2;
                        ret[binaryIdx] = (byte)(hexInt);
                    }
                    return ret;
                case SendKind.TextUtf8:
                    return Encoding.UTF8.GetBytes(baseText);
                case SendKind.TextSjis:
                    return Encoding.GetEncoding("Shift_JIS").GetBytes(baseText);
                default:
                    throw new NotSupportedException();
            }
        }
        /// <summary>
        /// バイナリデータを対応する文字列に変換します。
        /// </summary>
        /// <param name="kind"></param>
        /// <param name="baseData"></param>
        /// <returns></returns>
        public static string ConvertToText(this SendKind kind, byte[] baseData)
        {
            switch (kind)
            {
                case SendKind.Binary:
                    var ret = new StringBuilder();
                    foreach (var byteData in baseData)
                    {
                        for (int byteIdx = 0; byteIdx < 8; byteIdx++)
                        {
                            var filter = 1 << (7 - byteIdx);
                            ret.Append((byteData & filter) > 0 ? '1' : '0');
                        }
                    }
                    return ret.ToString();
                case SendKind.HexaDecimal:
                    ret = new StringBuilder();
                    for (int idx = 0; idx < baseData.Length; idx++)
                    {
                        int hexInt = baseData[idx];
                        ret.Append(hexInt.ToString("X2"));
                    }
                    return ret.ToString();
                case SendKind.TextUtf8:
                    return Encoding.UTF8.GetString(baseData);
                case SendKind.TextSjis:
                    return Encoding.GetEncoding("Shift_JIS").GetString(baseData);
                default:
                    throw new NotSupportedException();
            }
        }
        /// <summary>
        /// 送信種別に応じて書式を整形します。
        /// </summary>
        /// <param name="kind">送信種別</param>
        /// <param name="baseText">整形前の文字列</param>
        /// <param name="forDisplay">画面表示用の場合true</param>
        /// <returns></returns>
        public static string ToFormatText(this SendKind kind, string baseText, bool forDisplay = false)
        {
            // 2進・16進以外はそのまま
            if (kind != SendKind.Binary && kind != SendKind.HexaDecimal) return baseText;

            char[] keys = kind == SendKind.Binary ? new[] { '0', '1' }
                                                  : new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'a', 'b', 'c', 'd', 'e', 'f' };
            
            var text = new string(baseText.Where(c => keys.Contains(c)).ToArray());
            if (!forDisplay) return text;

            var sb = new StringBuilder();
            for (int idx = 0; idx < text.Length; idx++)
            {
                sb.Append(text[idx]);
                if (idx == 0 || idx == text.Length - 1) continue;

                // 行末以外は4文字区切りでスペースを挟む
                if (idx % 4 == 3) sb.Append(' ');
            }
            return sb.ToString();
        }
    }
}
