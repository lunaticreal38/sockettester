﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketTester
{
    /// <summary>
    /// 応答情報を返します。
    /// </summary>
    public class ResponseItem
    {
        /// <summary>
        /// 画面表示用のタイトルを取得します。
        /// </summary>
        public string ResponseTitle { get { return string.Format("{0:HH:mm:ss.ffffff}-[{1}]", ResponseDate, title); } }
        /// <summary>
        /// 応答日時を取得します。
        /// </summary>
        public DateTime ResponseDate { get; }
        /// <summary>
        /// 応答データを取得します。
        /// </summary>
        public byte[] Data { get; }


        private string title;


        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="title">タイトル</param>
        /// <param name="resopnseDate">受信日時</param>
        /// <param name="data">受信データ ※表示しない場合は省略可</param>
        public ResponseItem(string title, DateTime resopnseDate, byte[] data = null)
        {
            this.title = title;
            this.ResponseDate = resopnseDate;
            this.Data = data ?? new byte[0];
        }

    }
}
