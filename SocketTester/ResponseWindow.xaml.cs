﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SocketTester
{
    /// <summary>
    /// レスポンス受信ウィンドウ画面
    /// </summary>
    public partial class ResponseWindow : Window
    {
        /// <summary>
        /// 画面が閉じられているかどうかを取得します。
        /// </summary>
        public bool IsClosed { get; private set; }
        /// <summary>
        /// 応答情報リストを取得・設定します。
        /// </summary>
        public ObservableCollection<ResponseItem> ResponseList
        {
            get { return (ObservableCollection<ResponseItem>)GetValue(ResponseListProperty); }
            set { SetValue(ResponseListProperty, value); }
        }
        /// <summary>
        /// 応答情報リスト依存プロパティ
        /// </summary>
        public static readonly DependencyProperty ResponseListProperty =
            DependencyProperty.Register("ResponseList", typeof(ObservableCollection<ResponseItem>), typeof(ResponseWindow));


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ResponseWindow()
        {
            InitializeComponent();
            ResponseList = new ObservableCollection<ResponseItem>();
            this.DataContext = this;
        }


        /// <summary>
        /// 応答情報をリストに追加します。
        /// </summary>
        /// <param name="item"></param>
        public void AddResponseItem(ResponseItem item)
        {
            if (!App.Current.Dispatcher.CheckAccess())
            {
                // ディスパッチャから再帰呼び出し
                App.Current.Dispatcher.Invoke(new Action<ResponseItem>(AddResponseItem), item);
                return;
            }
            ResponseList.Insert(0, item);
        }


        /// <summary>
        /// WindowClose時処理
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            IsClosed = true;
        }
    }
}
