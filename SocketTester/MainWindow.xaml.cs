﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SocketTester
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// 接続状態を取得します。
        /// </summary>
        public bool IsConnected
        {
            get { return (bool)GetValue(IsConnectedProperty); }
            private set { SetValue(IsConnectedProperty, value); }
        }
        /// <summary>
        /// 接続状態依存プロパティです。
        /// </summary>
        public static readonly DependencyProperty IsConnectedProperty =
            DependencyProperty.Register("IsConnected", typeof(bool), typeof(MainWindow));
        /// <summary>
        /// 処理実行中かどうかを取得します。
        /// </summary>
        public bool IsWorking
        {
            get { return (bool)GetValue(IsWorkingProperty); }
            private set { SetValue(IsWorkingProperty, value); }
        }
        public static readonly DependencyProperty IsWorkingProperty =
            DependencyProperty.Register("IsWorking", typeof(bool), typeof(MainWindow));


        private TcpClient tcpClient;
        private ResponseWindow responseWindow;


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;

            // コマンドパラメータの先頭からURL,ユーザ,パスワードを読み込んで入力欄にセット
            var args = Environment.GetCommandLineArgs();

            if (args.Length >= 2) this.hostTxt.Text = args[1];
            if (args.Length >= 3) this.portTxt.Text = args[2];

        }


        /// <summary>
        /// UIスレッド上で表示欄のテキストを設定します。
        /// </summary>
        /// <param name="text">表示文字列</param>
        private void SetLogTxt(string text)
        {
            if (!App.Current.Dispatcher.CheckAccess())
            {
                // ディスパッチャから再帰呼び出し
                App.Current.Dispatcher.Invoke(new Action<string>(SetLogTxt), text);
                return;
            }
            this.logTxt.Text = string.Format("{0:yyyy/MM/dd HH:mm:ss.ffffff}{1}{1}{2}", DateTime.Now, Environment.NewLine, text);
        }
        /// <summary>
        /// TCPClientソケットが接続中かどうかを返します。
        /// </summary>
        /// <returns></returns>
        private bool IsConnectedTcpClient()
        {
            return tcpClient?.Client?.Connected ?? false;
        }
        /// <summary>
        /// 応答受信をバックグラウンドで実行します。
        /// パラメータどちらかがnullの場合はキャンセルします。
        /// 回線切断されるか応答ウィンドウを閉じた場合にタスク終了します。
        /// </summary>
        /// <param name="client"></param>
        /// <param name="window"></param>
        private async void RunResponsePolling(TcpClient client, ResponseWindow window)
        {
            // TCP接続と応答ウィンドウ表示が両立していなければ開始しない
            if (client == null || window == null) return;

            SetLogTxt("レスポンス待受開始");
            var res = await Task.Factory.StartNew(() =>
            {
                if (!window.IsClosed) window.AddResponseItem(new ResponseItem("待受開始", DateTime.Now));
                while (true)
                {
                    try
                    {
                        if (!IsConnectedTcpClient()) return "回線が切断されました。";
                        if (window.IsClosed) return "応答受信ウィンドウが閉じられました。";

                        var available = client?.Available ?? 0;
                        if (available > 0)
                        {
                            var data = new byte[available];
                            client.GetStream().Read(data, 0, available);
                            if (!window.IsClosed) window.AddResponseItem(new ResponseItem("受信", DateTime.Now, data));
                        }
                    }
                    catch (Exception ex)
                    {
                        SetLogTxt("レスポンス受信エラー" + Environment.NewLine + ex);
                    }
                    Thread.Sleep(1000);
                }
            });

            if (!window.IsClosed) window.AddResponseItem(new ResponseItem("待受終了", DateTime.Now));
            SetLogTxt("レスポンス受信終了" + Environment.NewLine + res);
        }


        #region イベント
        /// <summary>
        /// 接続ボタンクリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void connectionBtn_Click(object sender, RoutedEventArgs e)
        {
            // 先に接続状態を確認し、接続中なら切断する
            if (IsConnectedTcpClient())
            {
                tcpClient.Close();
                tcpClient.Dispose();
                tcpClient = null;
                IsConnected = false;
                SetLogTxt("切断");
                return;
            }

            var host = this.hostTxt.Text.Trim();
            var port = int.TryParse(this.portTxt.Text, out int tmpPort) ? tmpPort : -1;

            if (string.IsNullOrEmpty(host) || port < 0)
            {
                SetLogTxt("ホスト名：ポートの値が不正です");
                return;
            }
            try
            {
                IsWorking = true;
                SetLogTxt("接続中・・・");
                tcpClient = new TcpClient();
                await tcpClient.ConnectAsync(this.hostTxt.Text, port);
                SetLogTxt("接続成功");

                // 応答待ち受けウィンドウが開かれていれば待ち受けを開始する
                RunResponsePolling(tcpClient, responseWindow);
            }
            catch (Exception ex)
            {
                SetLogTxt("接続失敗" + Environment.NewLine + Environment.NewLine + ex);
            }
            finally
            {
                IsWorking = false;
                IsConnected = IsConnectedTcpClient();
            }
        }
        /// <summary>
        /// 送信ボタンクリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            if (!IsConnectedTcpClient())
            {
                IsConnected = false;
                SetLogTxt("回線が切断されています。");
                return;
            }

            try
            {
                IsWorking = true;
                SetLogTxt("送信中・・・");

                // 送信用の体裁に整えてからbyte変換
                var sendKind = (SendKind)this.sendKindCmb.SelectedValue;
                var text = sendKind.ToFormatText(this.requestTxt.Text);
                var sendData = sendKind.ConvertToData(text);
                var now = DateTime.Now;
                var resultMsg = string.Empty;
                try
                {
                    var stream = tcpClient.GetStream(); // このStreamは待受でも利用するためDisposeしない
                    stream.Write(sendData, 0, sendData.Length);
                    resultMsg = "通信成功";
                }
                catch (Exception ex)
                {
                    resultMsg = "通信失敗" + Environment.NewLine + ex;
                }
                var edDate = DateTime.Now;
                SetLogTxt(string.Format("所要時間 {0}秒{1}{2}", (edDate - now).TotalSeconds.ToString("F2"), Environment.NewLine, resultMsg));
            }
            catch (Exception ex)
            {
                SetLogTxt("通信失敗" + Environment.NewLine + ex);
            }
            finally
            {
                IsWorking = false;
            }
        }
        /// <summary>
        /// ドラッグオーバーイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void requestTxt_PreviewDragOver(object sender, DragEventArgs e)
        {
            try
            {
                e.Effects = DragDropEffects.Copy;
                e.Handled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        /// <summary>
        /// ドロップイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void requestTxt_Drop(object sender, DragEventArgs e)
        {
            try
            {
                string[] tmpFileName = e.Data.GetData("FileNameW") as string[];
                var filePath = tmpFileName[0];
                if (!File.Exists(filePath)) return; // ファイルでなければ何もしない

                // 送信種別に対応するエンコードで読み込むようにしておく
                Encoding enc;
                switch ((SendKind)this.sendKindCmb.SelectedValue)
                {
                    case SendKind.TextUtf8:
                        enc = Encoding.UTF8;
                        break;
                    case SendKind.TextSjis:
                        enc = Encoding.GetEncoding("Shift_JIS");
                        break;
                    default:
                        // その他はUTF-8にしておく
                        enc = Encoding.UTF8;
                        break;
                }
                this.requestTxt.Text = File.ReadAllText(filePath, enc);
            }
            catch (Exception ex)
            {
                SetLogTxt(ex.ToString());
            }
        }
        /// <summary>
        /// テキスト整形ボタンクリックイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SeikeiButton_Click(object sender, RoutedEventArgs e)
        {
            var sendKind = ((ComboBoxItem)this.sendKindCmb.SelectedItem).Tag as SendKind? ?? SendKind.Unknown;
            this.requestTxt.Text = sendKind.ToFormatText(this.requestTxt.Text, true);
        }
        /// <summary>
        /// 応答監視ボタンクリックイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResponseWindowButton_Click(object sender, RoutedEventArgs e)
        {
            if (responseWindow != null) return;
            responseWindow = new ResponseWindow();
            responseWindow.Owner = this;
            responseWindow.Top = responseWindow.Owner.Top;
            responseWindow.Left = responseWindow.Owner.Left + this.ActualWidth;
            responseWindow.Closed += (_, __) =>
            {
                responseWindow = null;
            };
            responseWindow.Show();
            RunResponsePolling(tcpClient, responseWindow);
        }
        #endregion
    }
}