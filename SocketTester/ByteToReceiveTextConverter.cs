﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace SocketTester
{
    /// <summary>
    /// バイト配列を応答文字列へ変換するコンバータ
    /// </summary>
    public class ByteToReceiveTextConverter : DependencyObject, IMultiValueConverter
    {
        /// <summary>
        /// 変換対象のbyte配列と送信種別を使用して文字列変換します。
        /// </summary>
        /// <param name="values">byte配列、送信種別、整形フラグ</param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 3
                || values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue
                || values[0] == null || values[1] == null) return null;
            if (!(values[0] is byte[])) throw new ArgumentException("Values[0]型不正");
            if (!(values[1] is SendKind)) throw new ArgumentException("Values[1]型不正");

            var sendKind = (SendKind)values[1];
            var text = sendKind.ConvertToText((byte[])values[0]);
            var useFormat = true.Equals(values[2]);
            return useFormat ? sendKind.ToFormatText(text, true) : text;
        }
        /// <summary>
        /// 使用しない
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetTypes"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
