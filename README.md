# OverView

SocketTesterはWPF TCPClientを使用したテキストベースの送受信検証ツールです。

サーバサイドの動作検証を目的としています。

![overview](https://gitlab.com/lunaticreal38/sockettester/uploads/1a05e80d8f64e68160383879d2479ee0/sockettester-logo.png)


## Install

ビルドにはVisualStudio2019以降を使用してください。

実行環境は.Net Framework 4.7.2 以上が対象です。

ビルドして生成されたSocketTester.exeをクリックするとアプリケーション起動します。


## Usage

### Send

IPAddress:Portに接続先を入力し、「接続」をクリックします。

【送信】に送信するデータを文字列で入力し、送受信形式を指定して「送信」をクリックすると接続先へデータ送信します。

ex)
``` 
【送信】`0000 0001 0002...` 送受信形式「2進」
【送信】`01 23 AB CD FF...` 送受信形式「16進」
```

※データ転送はBigEndianのみ対応です


### Response

「応答監視」をクリックすると別ウィンドウで監視を行います。
応答受信ウィンドウを表示していない場合や通信が切断されている場合は監視は止まります。

